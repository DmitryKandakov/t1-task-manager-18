package ru.t1.dkandakov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFiledException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
