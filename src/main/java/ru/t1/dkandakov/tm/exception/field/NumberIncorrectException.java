package ru.t1.dkandakov.tm.exception.field;

public class NumberIncorrectException extends AbstractFiledException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value) {
        super("Error! Value\"" + value + "\"is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! Value\"" + value + "\"is incorrect...");
    }

}
